;<?php/*

[general]

name                        ="LibPdf"
version                     ="6.2.8.7"
encoding                    ="UTF-8"
description                 ="Ovidentia Library for PDF files"
description.fr              ="Librairie partagée fournissant une API de génération de fichiers PDF"
long_description.fr         ="README.md"
delete                      =1
ov_version                  ="8.1.98"
php_version                 ="5.1.0"
addon_access_control        ="0"
author                      ="Paul de Rosanbo (paul.derosanbo@cantico.fr)"
mod_gd2                     ="Available"
upload_directory            ="Available"
mysql_character_set_database="latin1,utf8"
icon                        ="pdf.png"
tags                        ="library,pdf"
;*/?>
