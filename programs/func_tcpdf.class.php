<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

/**
 * Translate string
 * TODO : create translation file for the addon
 */
function LibPdf_translate($str) {
	return bab_translate($str);
}



class Func_TcPdf extends bab_functionality {

	/**
	 * Get description of functionality
	 * @return string
	 */
	public function getDescription() {
		return LibPdf_translate('Create PDF files with TCPDF');
	}

	/**
	* Construct a TCPDF object.
	* It allows to set up the page format, the orientation and
	* the measure unit used in all the methods (except for the font sizes).
	*
	* @param string $orientation page orientation. Possible values are (case insensitive):<ul><li>P or Portrait (default)</li><li>L or Landscape</li></ul>
	* @param string $unit User measure unit. Possible values are:<ul><li>pt: point</li><li>mm: millimeter (default)</li><li>cm: centimeter</li><li>in: inch</li></ul><br />A point equals 1/72 of inch, that is to say about 0.35 mm (an inch being 2.54 cm). This is a very common unit in typography; font sizes are expressed in that unit.
	* @param mixed $format The format used for pages. It can be either one of the following values (case insensitive) or a custom format in the form of a two-element array containing the width and the height (expressed in the unit given by unit).<ul><li>4A0</li><li>2A0</li><li>A0</li><li>A1</li><li>A2</li><li>A3</li><li>A4 (default)</li><li>A5</li><li>A6</li><li>A7</li><li>A8</li><li>A9</li><li>A10</li><li>B0</li><li>B1</li><li>B2</li><li>B3</li><li>B4</li><li>B5</li><li>B6</li><li>B7</li><li>B8</li><li>B9</li><li>B10</li><li>C0</li><li>C1</li><li>C2</li><li>C3</li><li>C4</li><li>C5</li><li>C6</li><li>C7</li><li>C8</li><li>C9</li><li>C10</li><li>RA0</li><li>RA1</li><li>RA2</li><li>RA3</li><li>RA4</li><li>SRA0</li><li>SRA1</li><li>SRA2</li><li>SRA3</li><li>SRA4</li><li>LETTER</li><li>LEGAL</li><li>EXECUTIVE</li><li>FOLIO</li></ul>
	* @param boolean $unicode TRUE means that the input text is unicode (default = true)
	* @param String $encoding charset encoding; default is UTF-8
	*
	* @return TCPDF
	*/
	public function createDocument($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding="UTF-8") {

		$this->includeTCPDF();
		$tcpdf = new TCPDF($orientation, $unit, $format, $unicode, $encoding);

		// set default font to embeded font
		$tcpdf->SetFont('dejavusans');

		$tcpdf->getAliasNbPages();

		// set language to ovidentia configuration
		$tcpdf->setLanguageArray($this->getLanguageArray());

		return $tcpdf;
	}

	/**
	 * Include necessary files to extends TCPDF
	 *
	 * @return boolean
	 */
	public function includeTCPDF() {
		if (!defined('K_TCPDF_EXTERNAL_CONFIG')) {

			$addonInfo = bab_getAddonInfosInstance('LibPdf');

			// ensure created upload path for images cache creation by TCPDF
			$ulPath = new bab_Path($addonInfo->getUploadPath());
			$ulPath->createDir();

			define('K_TCPDF_EXTERNAL_CONFIG'	, 1);

			define("K_PATH_MAIN"				, dirname(__FILE__).'/tcpdf/');
			define("K_PATH_URL"					, $addonInfo->getUrl() . $GLOBALS['babInstallPath'] . 'addons/LibPdf/tcpdf/');
			define("K_PATH_FONTS"				, K_PATH_MAIN."fonts/");
			define("K_PATH_CACHE"				, $addonInfo->getUploadPath());
			define("K_PATH_URL_CACHE"			, K_PATH_CACHE);
			define("K_PATH_IMAGES"				, '');
			define("K_BLANK_IMAGE"				, '');

			/**
			 * height of cell repect font height
			 */
			define("K_CELL_HEIGHT_RATIO"		, 1.25);

			/**
			 * reduction factor for small font
			 */
			define("K_SMALL_RATIO"				, 2/3);

			/**
			 * define default PDF document producer
			 */
			define('PDF_PRODUCER'				,'TCPDF 4.0.033 / Ovidentia addon LibPDF / www.cantico.fr');

			require_once dirname(__FILE__).'/tcpdf/tcpdf.php';
			return true;
		}

		return false;
	}


	/**
	 * Include necessary files to extends FPDI
	 *
	 * @since 6.2.8.7
	 */
	public function includeFPDI()
	{
	    require_once dirname(__FILE__).'/fpdi/fpdi.php';
	}


	/**
	 * Create a language array compatible with TCPDF configuration format
	 * @return array
	 */
	public function getLanguageArray() {
		return array(
			'a_meta_charset'	=> 'UTF-8',
			'a_meta_dir'		=> 'ltr',
			'a_meta_language'	=> bab_getLanguage(),
			'w_page'			=> LibPdf_translate('page')
		);
	}




	/**
	 * Helper object to create table with MultiCell method
	 * @return LibPdf_table
	 */
	public function getTableHelper() {
		require_once dirname(__FILE__).'/table.class.php';
		return new LibPdf_table;
	}



	protected function getFixedFontStyle($text, $fontstyle, $isHtml)
	{

	    if ($isHtml) {
	        $m = null;
	        $text = preg_replace('/\s\s+/', ' ', $text);

	        if (preg_match('/<strong>.+<\/strong>/', $text, $m) && false === strpos($fontstyle, 'B'))
	        {
	            $fontstyle .= 'B';
	        }

	        if (preg_match('/<em>.+<\/em>/', $text, $m) && false === strpos($fontstyle, 'I'))
	        {
	            $fontstyle .= 'I';
	        }
	    }

	    return $fontstyle;
	}


	/**
	 * Split a word
	 * @param TCPDF $tcpdf
	 * @param string 	$text
	 * @param int 		$width
	 * @param string 	$fontname
	 * @param string 	$fontstyle
	 * @param int 		$fontsize
	 *
	 * @return array
	 */
	protected function getWordLines(TCPDF $tcpdf, $word, $width, $fontname, $fontstyle, $fontsize)
	{
	    $lines = array();
	    $wordWidth = $tcpdf->GetStringWidth($word, $fontname, $fontstyle, $fontsize);

	    if ($wordWidth <= $width) {
	        return null;

	    } else {

	        $currentLine = '';

	        // split $currentLine by char
	        $chars = preg_split('//', $word, -1, PREG_SPLIT_NO_EMPTY);
	        foreach ($chars as $c) {
	            $cumulatedWidth = $tcpdf->GetStringWidth($currentLine.$c, $fontname, $fontstyle, $fontsize);
	            if ($cumulatedWidth <= $width) {
	                $currentLine .= $c;
	            } else {
	                $lines[] = $currentLine;
	                $currentLine = $c;
	            }
	        }

	        if ('' !== $currentLine) {
	            $lines[] = $currentLine;
	        }
	    }



	    return $lines;
	}



	/**
	 * Split a text without new lines in an array of strings where each string is smaller than the specified width.
	 * @since 0.0.2
	 *
	 * @param TCPDF $tcpdf
	 * @param string 	$text
	 * @param int 		$width
	 * @param string 	$fontname
	 * @param string 	$fontstyle
	 * @param int 		$fontsize
	 * @param bool		$isHtml		Only <strong> <em> on complete lines
	 * @return array		The text exploded in an array of strings so that each string has a width inferior to the cell width.
	 */
	private function splitSingleLineText(TCPDF $tcpdf, $text, $width, $fontname='', $fontstyle='', $fontsize=0, $isHtml=false)
	{
	    if ($width <= 0) {
	        throw new Exception('Failed to split with a 0 width');
	    }

		if ($text === '') {
			return array('');
		}




		$words = explode(' ', $text);

		$lines = array();
		$currentLine = '';

		foreach ($words as $word) {
			if ($currentLine !== '') {
				$space = ' ';
			} else {
				$space = '';
			}
			$newLineWidth = $tcpdf->GetStringWidth($currentLine . $space . $word, $fontname, $fontstyle, $fontsize);
			if ($newLineWidth <= $width) {
			    // add word to current line
				$currentLine .= $space . $word;


			} else {

			    $wordLines = $this->getWordLines($tcpdf, $word, $width, $fontname, $fontstyle, $fontsize);

			    if (isset($wordLines)) {
			        $currentLine = array_pop($wordLines);
			        $lines = array_merge($lines, $wordLines);

			    } elseif ($currentLine !== '') {
			        $lines[] = $currentLine;
			        $currentLine = $word;
			    }



			}
		}

		if ($currentLine !== '') {
			$lines[] = $currentLine;
		}

		return $lines;
	}


	/**
	 * @return array
	 */
	protected function splitParagraphs($text, $isHtml)
	{
	    if ($isHtml) {
	        return preg_split('/<br[\s\/]*>/', $text, -1);
	    } else {
	        return explode("\n", $text);
	    }
	}


	/**
	 * Split a text in an array of strings where each string has a smaller width than the one specified.
	 * @since 0.0.2
	 *
	 * @param TCPDF 	$tcpdf
	 * @param string 	$text
	 * @param int 		$width
	 * @param string 	$fontname
	 * @param string 	$fontstyle
	 * @param int 		$fontsize
	 * @param bool 		$isHtml		 Only <br /> <strong> <em>
	 * @return array	The text exploded in an array of strings so that each string has a width inferior to the cell width.
	 */
	public function splitText(TCPDF $tcpdf, $text, $width, $fontname='', $fontstyle='', $fontsize=0, $isHtml=false)
	{
	    $paragraphs = $this->splitParagraphs($text, $isHtml);
	    $fontstyle = $this->getFixedFontStyle($text, $fontstyle, $isHtml);

		$lines = array();
		foreach ($paragraphs as $paragraph) {
			$lines = array_merge($lines, $this->splitSingleLineText($tcpdf, $paragraph, $width, $fontname, $fontstyle, $fontsize, $isHtml));
		}

		return $lines;
	}




	/**
	 * @return int
	 */
	protected function getLineLongestWordSize(TCPDF $tcpdf, $text, $fontname='', $fontstyle='', $fontsize=0, $isHtml=false)
	{
	    $words = explode(' ', $text);
	    $max = 0;

	    foreach ($words as $word) {

	        $wordWidth = $tcpdf->GetStringWidth($word, $fontname, $fontstyle, $fontsize);

	        if ($wordWidth > $max) {
	            $max = $wordWidth;
	        }
	    }

	    return $max;
	}



	public function getLongestWordSize(TCPDF $tcpdf, $text, $fontname='', $fontstyle='', $fontsize=0, $isHtml=false)
	{
	    $paragraphs = $this->splitParagraphs($text, $isHtml);
	    $fontstyle = $this->getFixedFontStyle($text, $fontstyle, $isHtml);

	    $max = 0;
	    foreach ($paragraphs as $paragraph) {
	        $ps = $this->getLineLongestWordSize($tcpdf, $paragraph, $fontname, $fontstyle, $fontsize, $isHtml);

	        if ($ps > $max) {
	            $max = $ps;
	        }
	    }

	    return $max;
	}
}
