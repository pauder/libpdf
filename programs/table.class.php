<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

class LibPdf_column {


	private $cellwidth 				= array(30	,'%');
	private $width					= null;
	private	$table					= null;

	private $alignment				= 'L';


	/**
	 * @since 6.2.8.5
	 * @param	int		$width		if width is = 0, the cell will take all availaible space
	 * 								if width is = -1 the cells will be sized to the minimum width according to content
	 * @param	string 	$unit		empty string is the unit of the pdf document, '%' is in percent from available space on one page
	 * @return LibPdf_column
	 */
	public function setCellWidth($width, $unit = '')
	{
		$this->cellwidth = array($width, $unit);
		return $this;
	}

	/**
	 * @deprecated     Use setCellWidth
	 */
	public function setCellWith($width, $unit = '')
	{
	    return $this->setCellWidth($width, $unit);
	}

	public function getWidth($available_width) {
		if (null === $this->width) {
			$this->width = LibPdf_table::getSize($this->cellwidth, $available_width);
		}
		return $this->width;
	}

	public function setWidth($width) {
		$this->width = $width;
	}

	public function setTable($table) {
		$this->table = $table;
	}

	public function getTable() {
		return $this->table;
	}

	public function setAlignment($alignment) {
		$this->alignment = $alignment;
		return $this;
	}

	public function getAlignment() {
		return $this->alignment;
	}
}




class LibPdf_row {


	public $pdf					= null;
	public $available_width 	= 0;

	/**
	 * @var LibPdf_cell[]
	 */
	private $cells 				= array();
	private $table 				= null;
	private $textcolor 	        = null;
	private $backgroundcolor 	= array(255, 255, 255);
	private $fontStyle			= '';
	private $border				= null;
	private	$height				= null;

	private $ishtml				= null;
	private $cellpadding		= null;


	/**
	 *
	 * @param LibPdf_column $column
	 * @return LibPdf_cell
	 */
	public function createCell(LibPdf_column $column = null)
	{
		$cell = new LibPdf_cell;
		$cell->setRow($this);
		if (isset($column)) {
		    $cell->inColumn($column);
		}
		$this->cells[] = $cell;

		return $cell;
	}


	/**
	 * @return LibPdf_row
	 */
	public function setHeight($height) {

		$this->height = $height;
		return $this;
	}

	public function getHeight(TCPDF $pdf = null) {


		if (isset($this->height)) {
			return $this->height;
		}

		if (isset($this->table)) {
			$tablecellheight = $this->table->getCellHeight();
			if (null !== $tablecellheight) {
				return LibPdf_table::getSize($tablecellheight, $this->table->getHeight());
			}
		}

		$maxheight = 0;
		foreach($this->cells as $cell) {
			/*@var $cell LibPdf_cell */
			$contentheight = $cell->getContentHeight($pdf);
			if ($maxheight < $contentheight) {
				$maxheight = $contentheight;
			}
		}

		return $maxheight;
	}

	/**
	 *
	 * @return LibPdf_cell[]
	 */
	public function getCells() {
		return $this->cells;
	}

	public function setTable($table) {
		$this->table = $table;
	}

	public function getTable() {
		return $this->table;
	}

	/**
	 * @return LibPdf_row
	 */
	public function setBackgroundColor($r, $g, $b) {

		$this->backgroundcolor = array($r, $g, $b);
		return $this;
	}

	public function getBackgroundColor() {
		return $this->backgroundcolor;
	}


	/**
	 * @return LibPdf_row
	 */
	public function setTextColor($r, $g, $b) {

		$this->textcolor = array($r, $g, $b);
		return $this;
	}

	public function getTextColor() {
		return $this->textcolor;
	}


	/**
	 * @return LibPdf_row
	 */
	public function setFontStyle($style) {

		$this->fontStyle = $style;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getFontStyle() {
		return $this->fontStyle;
	}


	/**
	 * @return LibPdf_row
	 */
	public function setBorder($border) {

		$this->border = $border;
		return $this;
	}


	/**
	 * @return mixed
	 */
	public function getBorder() {

		if (null === $this->border) {
			return $this->table->getBorder();
		}

		return $this->border;
	}



	/**
	 * Get / Set isHtml value
	 * @param	bool	$set
	 */
	public function isHtml($set = null) {
		if (null === $set) {
			if (null === $this->ishtml) {
				return $this->table->isHtml();
			}

			return $this->ishtml;
		}

		$this->ishtml = $set;
		return $this;
	}


	/**
	 * @param	float	$padding
	 * @return LibPdf_table
	 */
	public function setCellPadding($padding) {
		$this->cellpadding = $padding;
		return $this;
	}

	/**
	 * @return float | null
	 */
	public function getCellPadding() {

		if (null === $this->cellpadding) {
			return $this->table->getCellPadding();
		}

		return $this->cellpadding;
	}
}



class LibPdf_cell {

	// next cell position

	const 	NEXT_CELL_TO_RIGHT		= 0;
	const	NEXT_CELL_LINE_LEFT		= 1;
	const	NEXT_CELL_BELOW			= 2;

	const	CELLPADDING				= 1.0;

	private $data 					= null;

	/**
	 *
	 * @var LibPdf_row
	 */
	private $row					= null;

	/**
	 * @var LibPdf_column[]
	 */
	private $columns 				= array();

	private $border					= null;
	private $backgroundcolor		= null;
	private $textcolor              = null;
	private $alignment				= null;
	private $fontStyle				= null;

	private $ishtml					= null;
	private $cellpadding			= null;

	/**
	 *
	 * @param string $value
	 * @return LibPdf_cell
	 */
	public function setData($value) {
		$this->data = $value;
		return $this;
	}

	/**
	 * Set string in ovidentia charset
	 * @since 6.2.8.2
	 * @param string $value in the ovidentia charset
	 */
	public function setText($value) {
	    return $this->setData(bab_convertStringFromDatabase($value, 'UTF-8'));
	}


	public function setRow(LibPdf_row $row) {
		$this->row = $row;
	}

	public function getData() {
		return $this->data;
	}

	/**
	 *
	 * @param LibPdf_column $col
	 * @return LibPdf_cell
	 */
	public function inColumn(LibPdf_column $col) {
		$this->columns[] = $col;
		return $this;
	}

	/**
	 * @since 6.2.8.2
	 * @return LibPdf_cell
	 */
	public function inAllColumns()
	{
	    $this->columns = $this->row->getTable()->getColumns();
	    return $this;
	}


	/**
	 *
	 * @return array	<LibPdf_column>
	 */
	public function getColumns()
	{
		return $this->columns;
	}

	public function getWidth() {
		$width = 0;
		foreach($this->columns as $col) {
			$width += $col->getWidth($this->row->available_width);
		}

		return $width;
	}



	public function getContentHeight(TCPDF $pdf = null)
	{
		if (null === $pdf) {
			if (null === $this->row->pdf) {
				trigger_error('the getContentHeight need an instance of TCPDF');
				return 0;
			}
			$pdf = $this->row->pdf;
		}

		$func = bab_functionality::get('TcPdf');
		/*@var $func Func_TcPdf */

 		$availableWidth = $this->getWidth() - 2 * $this->getCellPadding();

		if ($availableWidth <= 0) {
		    $nbLines = 1;
		} else {
		    $lines = $func->splitText($pdf, $this->getData(), $availableWidth, '', '', 0, $this->isHtml());
		    $nbLines = count($lines);
		}

		return 2 * $this->getCellPadding() + ($nbLines * $pdf->getFontSize() * $pdf->getCellHeightRatio());
	}

	/**
	 * Content with of a cell
	 * @param TCPDF $pdf
	 * @return float
	 */
	public function getContentWidth(TCPDF $pdf = null)
	{
		if (null === $pdf) {
			if (null === $this->row->pdf) {
				trigger_error('the getContentHeight need an instance of TCPDF');
				return 0;
			}
			$pdf = $this->row->pdf;
		}

		$func = bab_functionality::get('TcPdf');

		$width = $this->getWidth();

		if (-1 === $width || 0 === $width)
		{
			// auto with for column, split for smallest size
			$width = $func->getLongestWordSize($pdf, $this->data, '', $pdf->getFontStyle(), $pdf->getFontSize(), $this->isHtml());
		} else {
			$width = $this->getWidth() - (2 * $this->getCellPadding());
		}

		return $width;

	}



	private function getAlignment() {
		if (null !== $this->alignment) {
			return $this->alignment;
		}

		return reset($this->columns)->getAlignment();
	}

	/**
	 * Set text alignement
	 * @param	string	L|C|R	L:left, C:center, R:right
	 * @return LibPdf_cell
	 */
	public function setAlignment($alignment) {
		$this->alignment = $alignment;
		return $this;
	}



	/**
	 * @return LibPdf_cell
	 */
	public function setFontStyle($style) {

		$this->fontStyle = $style;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getFontStyle() {

		if (null === $this->fontStyle) {
			return $this->row->getFontStyle();
		}

		return $this->fontStyle;
	}


	/**
	 * @return LibPdf_cell
	 */
	public function setBorder($border) {

		$this->border = $border;
		return $this;
	}


	/**
	 * @return mixed
	 */
	public function getBorder() {

		if (null === $this->border) {
			return $this->row->getBorder();
		}

		return $this->border;
	}


	/**
	 * @return LibPdf_cell
	 */
	public function setBackgroundColor($r, $g, $b) {

		$this->backgroundcolor = array($r, $g, $b);
		return $this;
	}


	/**
	 * @return mixed
	 */
	public function getBackgroundColor() {

		if (null === $this->backgroundcolor) {
			return $this->row->getBackgroundColor();
		}

		return $this->backgroundcolor;
	}




	/**
	 * @return LibPdf_cell
	 */
	public function setTextColor($r, $g, $b) {

		$this->textcolor = array($r, $g, $b);
		return $this;
	}


	/**
	 * @return mixed
	 */
	public function getTextColor() {

		if (null === $this->textcolor) {
			return $this->row->getTextColor();
		}

		return $this->textcolor;
	}



	/**
	 * Get / Set isHtml value
	 * @param	bool	$set
	 */
	public function isHtml($set = null) {
		if (null === $set) {
			if (null === $this->ishtml) {
				return $this->row->isHtml();
			}

			return $this->ishtml;
		}

		$this->ishtml = $set;
		return $this;
	}



	/**
	 * @param	float	$padding
	 * @return LibPdf_table
	 */
	public function setCellPadding($padding) {
		$this->cellpadding = $padding;
		return $this;
	}

	/**
	 * @return float | null
	 */
	public function getCellPadding() {
		if (null === $this->cellpadding) {

			if (null !== $rowcellpadding = $this->row->getCellPadding()) {
				return $rowcellpadding;
			}

			$this->cellpadding = self::CELLPADDING;
		}

		return $this->cellpadding;
	}



	public function draw(TCPDF $pdf, $available_width)
	{

		$pdf->SetCellPadding($this->getCellPadding());

		$this->row->pdf = $pdf;
		$this->row->available_width = $available_width;

		$bg = $this->getBackgroundColor();
		$pdf->SetFillColor($bg[0], $bg[1], $bg[2]);
		$textColor = $this->getTextColor();
		if (isset($textColor)) {
		    $pdf->SetTextColor($textColor[0], $textColor[1], $textColor[2]);
		}

		$pdf->setFont($pdf->getFontFamily(), $this->getFontStyle(), $pdf->getFontSizePt());


		$pdf->MultiCell(
			$this->getWidth(),
			$this->row->getHeight($pdf),
			$this->getData(),
			$this->getBorder(),
			$this->getAlignment(), 			// Allows to center or align the text. Possible values are:
											//  * L or empty string: left align
											//	* C: center
											//	* R: right align
											//	* J: justification (default value when $ishtml=false)

			1, 								// Indicates if the cell background must be painted (1) or transparent (0). Default value: 0.

			self::NEXT_CELL_TO_RIGHT,		//

			'',								// x position in user units
			'',								// y position in user units

			true,							// if true reset the last cell height (default true).

			0,								// stretch carachter mode:
											//	* 0 = disabled
											//	* 1 = horizontal scaling only if necessary
											//	* 2 = forced horizontal scaling
											//	* 3 = character spacing only if necessary
											//	* 4 = forced character spacing

			$this->isHtml(),				// set to true if $txt is HTML content (default = false).
			true, 							// $autopadding
			0, 								// $maxh
			'T', 							// $valign (string) Vertical alignment of text (requires $maxh = $h > 0). Possible values are:<ul><li>T: TOP</li><li>M: middle</li><li>B: bottom</li></ul>. This feature works only when $ishtml=false.
			false							// $fitcell if true attempt to fit all the text within the cell by reducing the font size.
		);

	}





}











class LibPdf_table {

	private $cols 			= array();

	private $tablewidth 	= array(100	,'%');
	private $tableheight 	= array(0	,'');

	private $x 				= null;

	private $cellwidth 		= array(30	,'%');
	private $cellheight		= null;
	private	$height			= null;
	private $border			= 1;

	/**
	 * @var LibPdf_row[]
	 */
	private $rows			= array();

	private $headerBg		= array(255, 255, 255);
	private $rowsBg			= array(255, 255, 255);

	private $ishtml			= false;

	private $cellpadding	= null;


	/**
	 * @param	float	$padding
	 * @return LibPdf_table
	 */
	public function setCellPadding($padding) {
		$this->cellpadding = $padding;
		return $this;
	}

	/**
	 * @return float | null
	 */
	public function getCellPadding() {
		return $this->cellpadding;
	}


	/**
	 * @since 6.2.8.5
	 * @param int $width
	 * @param string $unit
	 *
	 * @return LibPdf_table
	 */
	public function setTableWidth($width, $unit = '') {

		$this->tablewidth = array($width, $unit);
		return $this;
	}

	/**
	 * @deprecated replaced by setTableWidth
	 * @see LibPdf_table::setTableWidth()
	 */
	public function setTableWith($width, $unit = '') {
		return $this->setTableWidth($width, $unit);
	}


	/**
	 * Sets the left position of the table.
	 *
	 * @param int $x
	 *
	 * @return LibPdf_table
	 */
	public function setX($x) {
		$this->x = $x;
		return $this;
	}

	/**
	 * @return LibPdf_table
	 */
	public function setTableHeight($height, $unit = '') {

		$this->tableheight = array($height, $unit);
		return $this;
	}


	/**
	 * @deprecated replaced by setCellWidth
	 * @return LibPdf_table
	 */
	public function setCellWith($width, $unit = '') {

	    return $this->setCellWidth($width, $unit);
	}

	/**
	 * @since 6.2.8.5
	 * @return LibPdf_table
	 */
	public function setCellWidth($width, $unit = '') {

	    $this->cellwidth = array($width, $unit);
	    return $this;
	}


	/**
	 * @return LibPdf_table
	 */
	public function setCellHeight($height, $unit = '') {

		$this->cellheight = array($height, $unit);
		return $this;
	}


	/**
	 * @return array
	 */
	public function getCellHeight() {
		return $this->cellheight;
	}



	/**
	 * Create a new column object
	 * @return LibPdf_column
	 */
	public function createColumn() {

		$col = new LibPdf_column;
		$col->setTable($this);
		$col->setCellWith($this->cellwidth[0], $this->cellwidth[1]);

		$this->cols[] = $col;

		return $col;
	}

	/**
	 * @since 6.2.8.2
	 * @return LibPdf_column[]
	 */
	public function getColumns()
	{
	    return $this->cols;
	}


	/**
	 * Add a row with array
	 * @return LibPdf_row
	 */
	public function createRow() {

		$row = new LibPdf_row;
		$row->setTable($this);
		$this->rows[] = $row;
		return $row;
	}



	/**
	 * @return int
	 */
	public function getHeight()
	{
		return $this->height;
	}


	public function getComputedHeight(TCPDF $pdf)
	{
		$func = bab_functionality::get('TcPdf');

		$totalTableHeight = 0;

		foreach ($this->rows as $row) {

			$maxheight = 0;
			foreach ($row->getCells() as $cell) {
				$contentheight = $cell->getContentHeight($pdf);

				if ($maxheight < $contentheight) {
					$maxheight = $contentheight;
				}
			}

			$totalTableHeight += $maxheight;
		}

		return $totalTableHeight;
	}


	/**
	 * @param TCPDF $pdf
	 * @return number
	 */
    public function getComputedWidth(TCPDF $pdf)
    {
        $maxWidth = 0;
        foreach ($this->rows as $row) {

            $rowWidth = 0;
            foreach ($row->getCells() as $cell) {
                $rowWidth += $cell->getContentWidth($pdf);
            }

            if ($rowWidth > $maxWidth) {
                $maxWidth = $rowWidth;
            }
        }

        return $maxWidth;
    }


	/**
	 * add table to PDF document
	 */
	public function createMultiCells(TCPDF $pdf)
	{
		$margins = $pdf->getMargins();

		$available_width = $pdf->getPageWidth() - $margins['left'] - $margins['right'];
		if ($size = LibPdf_table::getSize($this->tablewidth, $available_width)) {
			$available_width = $size;
		}

		$available_height = $pdf->getPageHeight() - $margins['top'] - $margins['bottom'];
		if ($size = LibPdf_table::getSize($this->tableheight, $available_height)) {
			$available_height = $size;
		}

		if (null === $this->height && null !== $this->cellheight) {
			$this->height = LibPdf_table::getSize($this->cellheight, $available_height);
		}




		// adjust auto col width

		$auto_width = array();

		foreach($this->cols as $col) {
			if (-1 === $col->getWidth($available_width)) {
				$auto_width[] = $col;
			}
		}

		if ($auto_width)
		{
			foreach($auto_width as $col) {
				$maxwidth = 0;
				foreach ($this->rows as $row) {
					/* @var $cell LibPdf_cell */
					foreach ($row->getCells() as $cell) {
						$_cols = $cell->getColumns();

						if (1 == count($_cols))
						{
							$cellcol = reset($_cols);
							if ($cellcol == $col)
							{
								$width = $cell->getContentWidth($pdf);
								$width += (2* $cell->getCellPadding()) + (2* $pdf->GetLineWidth());

								if ($width > $maxwidth)
								{
									$maxwidth = $width;
								}
							}
						}
					}
				}


				$col->setWidth($maxwidth);
			}
		}


		// set missing width on cols

		$total_used_width = 0;
		$missing_width = array();


		foreach($this->cols as $col) {
			if (0 === $col->getWidth($available_width)) {
				$missing_width[] = $col;
			} else {
				$total_used_width += $col->getWidth($available_width);
			}
		}

		if ($missing_width) {
			$new_width = $available_width - $total_used_width / count($missing_width);
			foreach($missing_width as $col) {
				$col->setWidth($new_width);
			}
		}





		// rows



		foreach ($this->rows as $row) {
			/*@var $row LibPdf_row */


			$height = $row->getHeight($pdf);
			$newposY = $pdf->getY() + $height;
			if ($newposY >= ($pdf->getPageHeight() - $pdf->getBreakMargin()))
			{
				$pdf->AddPage();
			}

			if (isset($this->x)) {
				$pdf->setX($this->x);
			}

			/* @var $cell LibPdf_cell */
			foreach ($row->getCells() as $cell) {
				$cell->draw($pdf, $available_width);
			}


			$pdf->Ln();

		}

	}







	/**
	 * @return	float
	 */
	public static function getSize($arrSize, $available) {
		$size = $arrSize[0];

		if (0 === $size) {
			return 0;
		}

		if ('%' === $arrSize[1]) {
			$size = ($size*$available) / 100;
		}

		return $size;
	}








	/**
	 * @return LibPdf_table
	 */
	public function setBorder($border) {

		$this->border = $border;
		return $this;
	}


	/**
	 * @return mixed
	 */
	public function getBorder() {
		return $this->border;
	}



	/**
	 * Get / Set isHtml value
	 * @param	bool	$set
	 */
	public function isHtml($set = null) {
		if (null === $set) {
			return $this->ishtml;
		}

		$this->ishtml = $set;
		return $this;
	}
}
